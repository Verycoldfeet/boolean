/* Beregner egenskaber ud fra boolsk algebra */
#include <iostream>
#include <string>
#include <vector>
using namespace std;

// Array til mænd - vektorer bruges i stedet for array så størrelsen nemt kan findes.
vector<string> M = {"Jens", "Viktor", "Birk", "Markus"};
// Array til børn
vector<string> C = {"Viktor", "Birk", "Markus", "Ida"};
// Array til venstrehåndede
vector<string> L = {"Birk", "Markus", "Dorte", "Marie"};

/* Undersøger om array indeholder element. */
bool contains(vector<string> liste, string element) {
  for (int i = 0; i < liste.size(); i++) {
    // Returner sand, hvis dette er elementet.
    if (liste[i] == element){
            return true;
    }
  }
  // Returner falsk, hvis den ikke blev fundet.
  return false;
}

/* Undersøger om name er hunkøn. */
bool isFemale(string name) {

  return !contains(M, name);
}

/* Undersøger om name er en dreng. */
bool isBoy(string name) {
  return contains(M, name) && contains(C, name);
}

/* Undersøger om name er kvinde eller barn */
bool isWomanOrChild(string name) {
  return !contains(M, name) || contains(C, name);
}

int main(){

    vector<string> test = {"Jens","Dorte","Markus"};

    for(int i = 0; i < test.size(); i++){
        cout << "Is " << test[i] << " a woman?" << endl;
        if(isFemale(test[i])){
            cout << "True" << endl;
        } else {
            cout << "False" << endl;
        }
    }

    for(int i = 0; i < test.size(); i++){
        cout << "Is " << test[i] << " a boy?" << endl;
        if(isBoy(test[i])){
            cout << "True" << endl;
        } else {
            cout << "False" << endl;
        }
    }

    for(int i = 0; i < test.size(); i++){
        cout << "Is " << test[i] << " a woman or a child?" << endl;
        if(isWomanOrChild(test[i])){
            cout << "True" << endl;
        } else {
            cout << "False" << endl;
        }
    }

    return 0;
}
